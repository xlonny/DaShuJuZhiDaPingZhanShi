var citys=['韶关市', '清远市','广州市', '深圳市', '惠州市','阳江市', '清远市', '茂名市','高州市','湛江市', '河源市','梅州市','揭阳市','汕头市','汕尾市','珠海市','云浮市','江门市','中山市','肇庆市','佛山市','东莞市'];

var  mapData=[];
var mathResult=[];
$.get('./scripts/gd.json',function(geoJson){
  echarts.registerMap('gd',geoJson);
  mapData = geoJson.features.map(function(item){
  return {
    name: item.properties.name,           
    geo: item.properties.center
  }
});
    echart_1();
    echart_2();
    echart_3();
    echart_map();
      
    //echart_1湖南货物收入
    function echart_1() {

        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('chart_1'));
        option = {
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c}件"
            },
            legend: {
                x: 'center',
                y: '15%',
                data: mapData.map(function(dataItem) {
                    return dataItem.name;
                })               ,
                icon: 'circle',
                textStyle: {
                    color: '#fff',
                }
            },
            calculable: true,
            series: [{
                name: '',
                type: 'pie',
                //起始角度，支持范围[0, 360]
                // startAngle: 0,
                //饼图的半径，数组的第一项是内半径，第二项是外半径
                radius : '45%',
                //支持设置成百分比，设置成百分比时第一项是相对于容器宽度，第二项是相对于容器高度
                center: ['60%', '65%'],
                //是否展示成南丁格尔图，通过半径区分数据大小。可选择两种模式：
                // 'radius' 面积展现数据的百分比，半径展现数据的大小。
                //  'area' 所有扇区面积相同，仅通过半径展现数据大小
                // roseType: 'radius',
                //是否启用防止标签重叠策略，默认开启，圆环图这个例子中需要强制所有标签放在中心位置，可以将该值设为 false。
                avoidLabelOverlap: true,
                label: {
                    normal: {
                        show: true,
                        formatter: '{b}{c}万件'
                    },
                    emphasis: {
                        show: true
                    }
                },
                labelLine: {
                    normal: {
                        show: true,
                        length2: 1,
                    },
                    emphasis: {
                        show: true
                    }
                },               
                data: mapData.map(function(dataItem) {
                    return {
                        name: dataItem.name,
                        value: Math.round(Math.random()*100)
                    };
                })               
            }]
        };
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        window.addEventListener("resize", function () {
            myChart.resize();
        });
    }

    //电子商务销售额、订单数
    function echart_2() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('chart_2'));
        myChart.clear();
        option = {
            title: {
                text: ''
            },
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data:['销售额','订单数'],
                textStyle:{
                    color: '#fff'
                },
                top: '8%'
            },
            grid: {
                top: '40%',
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            color: ['#FF4949','#FFA74D','#FFEA51','#4BF0FF','#44AFF0','#4E82FF','#584BFF','#BE4DFF','#F845F1'],
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: ['2020年9月','2020年10月','2020年11月','2020年12月','2021年1月'],
                splitLine: {
                    show: false
                },
                axisLine: {
                    lineStyle: {
                        color: '#fff'
                    }
                }
            },
            yAxis: {
                name: '',
                type: 'value',
                splitLine: {
                    show: false
                },
                axisLine: {
                    lineStyle: {
                        color: '#fff'
                    }
                }
            },
            series: [
                {
                    name:'销售额',
                    type:'line',
                    data:[3961.88, 4233.63, 4183.14, 3633.01, 3704.47]
                },
                {
                    name:'订单数',
                    type:'line',
                    data:[3374.76, 3364.76, 3274.76, 3371.82, 3259.87]
                }
            ]
        };
        myChart.setOption(option);
    }

    // echart_map中国地图
    function echart_map() {
        var myChart = echarts.init(document.getElementById('chart_map'));
        function showProvince() {

            var data = [
                {
                    name: '韶关市',
                    value: [50,'电商进农村示范县：<br/>项目承建企业24家<br/>电商服务站目前数量24个站点<br/>广西金岸科技有限公司等企业均以优秀标准验收<br/>主要推广特产富川脐橙...']
                },{
                    name: '清远市',
                    value: [50,'电商进农村示范县：<br/>项目承建企业24家<br/>电商服务站目前数量24个站点<br/>广西金岸科技有限公司等企业均以优秀标准验收<br/>主要推广特产富川脐橙...']
                },{
                    name: '梅州市',
                    value: [50,'电商进农村示范县：<br/>项目承建企业24家<br/>电商服务站目前数量24个站点<br/>广西金岸科技有限公司等企业均以优秀标准验收<br/>主要推广特产富川脐橙...']
                },
                {
                    name: '河源市',
                    value: [50,'电商进农村示范县：<br/>项目承建企业24家<br/>电商服务站目前数量24个站点<br/>广西金岸科技有限公司等企业均以优秀标准验收<br/>主要推广特产富川脐橙...']
                },{
                    name: '广州市',
                    value: [50,'电商进农村示范县：<br/>项目承建企业24家<br/>电商服务站目前数量24个站点<br/>广西金岸科技有限公司等企业均以优秀标准验收<br/>主要推广特产富川脐橙...']
                },{
                    name: '肇庆市',
                    value: [50,'电商进农村示范县：<br/>项目承建企业24家<br/>电商服务站目前数量24个站点<br/>广西金岸科技有限公司等企业均以优秀标准验收<br/>主要推广特产富川脐橙...']
                },{
                    name: '佛山市',
                    value: [50,'电商进农村示范县：<br/>项目承建企业24家<br/>电商服务站目前数量24个站点<br/>广西金岸科技有限公司等企业均以优秀标准验收<br/>主要推广特产富川脐橙...']
                },{
                    name: '东莞市',
                    value: [50,'电商进农村示范县：<br/>项目承建企业2家<br/>电商服务站目前数量94个站点<br/>广西乐村淘科技有限公司、广西国际电子商务中心<br/>主要推广的产品有西林沙糖桔、麻鸭、姜晶等地理标志保护产品']
                },{
                    name: '惠州市',
                    value: [50,'电商进农村示范县：<br/>项目承建企业24家<br/>电商服务站目前数量24个站点<br/>广西金岸科技有限公司等企业均以优秀标准验收<br/>主要推广特产富川脐橙...']
                },{
                    name: '云浮市',
                    value: [50,'电商进农村示范县：<br/>项目承建企业24家<br/>电商服务站目前数量24个站点<br/>广西金岸科技有限公司等企业均以优秀标准验收<br/>主要推广特产富川脐橙...']
                },{
                    name: '江门市',
                    value: [50,'电商进农村示范县：<br/>项目承建企业24家<br/>电商服务站目前数量24个站点<br/>广西金岸科技有限公司等企业均以优秀标准验收<br/>主要推广特产富川脐橙...']
                },{
                    name: '中山市',
                    value: [50,'电商进农村示范县：<br/>项目承建企业24家<br/>电商服务站目前数量24个站点<br/>广西金岸科技有限公司等企业均以优秀标准验收<br/>主要推广特产富川脐橙...']
                },{
                    name: '深圳市',
                    value: [50,'电商进农村示范县：<br/>项目承建企业24家<br/>电商服务站目前数量24个站点<br/>广西金岸科技有限公司等企业均以优秀标准验收<br/>主要推广特产富川脐橙...']
                },{
                    name: '珠海市',
                    value: [50,'项目承建企业1家<br/>电商服务站目前数量85个站点<br/>参加电商培训人数1500人<br/>特色粮经作物：包括蚕桑、火麻、龙骨花、中药材、红薯、马铃薯、小杂粮等。<br/>主要承建内容:县级服务中心建设、乡镇级服务站、村级服务点建设']
                },{
                    name: '阳江市',
                    value: [50,'项目承建企业24家<br/>电商服务站目前数量24个站点<br/>广西金岸科技有限公司等企业均以优秀标准验收<br/>主要推广特产富川脐橙...']
                },{
                    name: '茂名市',
                    value: [50,'项目承建企业1家<br/>电商服务站目前数量60个站点<br/>参加电商培训人数1500人<br/>特色产品：靖西绣球、靖西壮锦、靖西东利大香儒<br/>主要承建内容:靖西各镇乡村服务站点建设']
                },{
                    name: '湛江市',
                    value: [50,'电商进农村示范县：<br/>项目承建企业24家<br/>电商服务站目前数量24个站点<br/>广西金岸科技有限公司等企业均以优秀标准验收<br/>主要推广特产富川脐橙...']
                },{
                    name: '潮州市',
                    value: [50,'电商进农村示范县：<br/>项目承建企业24家<br/>电商服务站目前数量24个站点<br/>广西金岸科技有限公司等企业均以优秀标准验收<br/>主要推广特产富川脐橙...']
                },{
                    name: '揭阳市',
                    value: [50,'电商进农村示范县：<br/>项目承建企业24家<br/>电商服务站目前数量24个站点<br/>广西金岸科技有限公司等企业均以优秀标准验收<br/>主要推广特产富川脐橙...']
                },{
                    name: '汕头市',
                    value: [50,'电商进农村示范县：<br/>项目承建企业24家<br/>电商服务站目前数量24个站点<br/>广西金岸科技有限公司等企业均以优秀标准验收<br/>主要推广特产富川脐橙...']
                },{
                    name: '汕尾市',
                    value: [50,'电商进农村示范县：<br/>项目承建企业24家<br/>电商服务站目前数量24个站点<br/>广西金岸科技有限公司等企业均以优秀标准验收<br/>主要推广特产富川脐橙...']
                }
            ];           
            
            var max = 480,
                min = 9;
            var maxSize4Pin = 50,
                minSize4Pin = 20;
            var convertData = function (data) {
                var res = [];
                for (var i = 0; i < data.length; i++) {
                 let geoCoord=  mapData.filter(function(dataItem) {                   
                    if(data[i].name==dataItem.name)                   
                    return dataItem
                  });
                    
                   if(geoCoord){   
                                   
                      res.push({
                          // name: data[i].name,
                          value:[geoCoord[0].geo[0],geoCoord[0].geo[1],data[i].value[0],data[i].value[1]] ,                            
                      });
                  
                   }
                }   
                res.push({value:[113.50,23.63,100,'ssssssssssssssss']});        
                return res;
            };

            myChart.setOption(option = {
                tooltip: {
                    trigger: 'item',
                    formatter: function loadData(result){
                        return result.name+'<br />' +result.value[3];
                    }
                },
                geo: {
                    zoom:1.2, 
                    show: true,
                    map:'广东',
                    mapType: '广东',
                    label: {
                        normal: {
                            show : true,
                            textStyle:{color:"#4bf316"}
                        },
                        emphasis: {
                            show: true,
                            textStyle: {
                                color: '#fff'
                            }
                        }, 
                    },
                    roam: true,
                    itemStyle: {                        
                        normal: {
                            borderColor: 'rgba(147, 235, 248, 1)',
                            borderWidth: 2,
                            areaColor: {
                                type: 'radial',
                                x: 0.5,
                                y: 0.5,
                                r: 0.8,
                                colorStops: [{
                                    offset: 0,
                                    color: 'rgba(175,238,238, 0)' // 0% 处的颜色
                                }, {
                                    offset: 1,
                                    color: 'rgba(   47,79,79, .2)' // 100% 处的颜色
                                }],
                                globalCoord: false // 缺省为 false
                            },
                            shadowColor: 'rgba(128, 217, 248, 1)',
                            shadowOffsetX: -2,
                            shadowOffsetY: 2,
                            shadowBlur: 10
                        },
                        emphasis: {
                            areaColor: '#389BB7',
                            borderWidth: 0
                        }
                    }
                },
                series: [                    
                    {
                        name: '门店分布情况',
                        type: 'scatter',
                        coordinateSystem: 'geo',
                        symbol: 'pin',
                        symbolSize: function(val) {
                            var a = (maxSize4Pin - minSize4Pin) / (max - min);
                            var b = minSize4Pin - a * min;
                            b = maxSize4Pin - a * max;
                            return a * val[2] + b;
                        },
                        label: {
                            normal: {
                                formatter: '{b}',
                                show: true,
                                textStyle: {
                                    color: '#fff',
                                    fontSize: 10,
                                }
                            }
                        },
                        itemStyle: {
                            normal: {
                                color: 'red', //标志颜色
                            }
                        },
                        zlevel: 6,
                        data: convertData(data),
                    },
                    {
                        type: 'effectScatter',
                        coordinateSystem: 'geo',
                        data: convertData(data.sort(function (a, b) {
                            return b.value - a.value;
                        }).slice(0, 47)),
                        symbolSize: function (val) {
                            return val[2] / 10;
                        },
                        showEffectOn: 'render',
                        rippleEffect: {
                            brushType: 'stroke'
                        },
                        hoverAnimation: true,
                        itemStyle: {
                            normal: {
                                color: '#05C3F9',
                                shadowBlur: 10,
                                shadowColor: '#05C3F9'
                            }
                        },
                        zlevel: 1
                    },

                ]
            });
        }
        showProvince();
        window.addEventListener("resize", function () {
            myChart.resize();
        });
    }

    //echart_3货物周转量
    function echart_3() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('chart_3'));
        myChart.clear();
        option = {
            title: {
                text: ''
            },
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data:['君乐宝','贝因美','飞鹤','伊利','中通速递','申通快递','韵达快递'],
                textStyle:{
                    color: '#fff'
                },
                top: '8%'
            },
            grid: {
                top: '40%',
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            color: ['#FF4949','#FFA74D','#FFEA51','#4BF0FF','#44AFF0','#4E82FF','#584BFF','#BE4DFF','#F845F1'],
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: ['2018年9月','2018年10月','2018年11月','2018年12月','2019年1月'],
                splitLine: {
                    show: false
                },
                axisLine: {
                    lineStyle: {
                        color: '#fff'
                    }
                }
            },
            yAxis: {
                name: '单',
                type: 'value',
                splitLine: {
                    show: false
                },
                axisLine: {
                    lineStyle: {
                        color: '#fff'
                    }
                }
            },
            series: [
                {
                    name:'顺丰快递',
                    type:'line',
                    data:[3961, 4233, 4183, 3633, 3704]
                },
                {
                    name:'邮政速递',
                    type:'line',
                    data:[3374, 3364, 3274, 3371, 3259]
                },
                {
                    name:'百世快递',
                    type:'line',
                    data:[14, 15, 13, 14, 15]
                },
                {
                    name:'圆通速递',
                    type:'line',
                    data:[686,847,895,865,886]
                },
                {
                    name:'中通速递',
                    type:'line',
                    data:[6133, 6577, 7019,6821,7294]
                },
                {
                    name:'申通快递',
                    type:'line',
                    data:[509, 862, 1481,1552,1333]
                },
                {
                    name:'韵达快递',
                    type:'line',
                    data:[509, 900, 1350,1487,1600]
                }
            ]
        };
        myChart.setOption(option);
    }

    //湖南省飞机场
    function echart_5() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('chart_5'));

        function showProvince() {
                var geoCoordMap = {
                    '长沙黄花国际机场': [113.226512,28.192929],
                    '张家界荷花机场': [110.454598,29.107223],
                    '常德桃花源机场': [111.651508,28.921516],
                    '永州零陵机场': [111.622869,26.340994],
                    '怀化芷江机场': [109.714784,27.44615],
                };
                var data = [{
                        name: '长沙黄花国际机场',
                        value: 100
                    },
                    {
                        name: '张家界荷花机场',
                        value: 100
                    },
                    {
                        name: '常德桃花源机场',
                        value: 100
                    },
                    {
                        name: '永州零陵机场',
                        value: 100
                    },
                    {
                        name: '怀化芷江机场',
                        value: 100
                    }
                ];
                var max = 480,
                    min = 9; // todo 
                var maxSize4Pin = 100,
                    minSize4Pin = 20;
                var convertData = function (data) {
                    var res = [];
                    for (var i = 0; i < data.length; i++) {
                        var geoCoord = geoCoordMap[data[i].name];
                        if (geoCoord) {
                            res.push({
                                name: data[i].name,
                                value: geoCoord.concat(data[i].value)
                            });
                        }
                    }
                    return res;
                };

                myChart.setOption(option = {
                    title: {
                        top: 20,
                        text: '',
                        subtext: '',
                        x: 'center',
                        textStyle: {
                            color: '#ccc'
                        }
                    },
                    legend: {
                        orient: 'vertical',
                        y: 'bottom',
                        x: 'right',
                        data: ['pm2.5'],
                        textStyle: {
                            color: '#fff'
                        }
                    },
                    visualMap: {
                        show: false,
                        min: 0,
                        max: 500,
                        left: 'left',
                        top: 'bottom',
                        text: ['高', '低'], // 文本，默认为数值文本
                        calculable: true,
                        seriesIndex: [1],
                        inRange: {
                        }
                    },
                    geo: {
                        show: true,
                        map:'hunan',
                        mapType: 'hunan',
                        label: {
                            normal: {
                            },
                            //鼠标移入后查看效果
                            emphasis: {
                                textStyle: {
                                    color: '#fff'
                                }
                            }
                        },
                        //鼠标缩放和平移
                        roam: true,
                        itemStyle: {
                            normal: {
                                //          	color: '#ddd',
                                borderColor: 'rgba(147, 235, 248, 1)',
                                borderWidth: 1,
                                areaColor: {
                                    type: 'radial',
                                    x: 0.5,
                                    y: 0.5,
                                    r: 0.8,
                                    colorStops: [{
                                        offset: 0,
                                        color: 'rgba(175,238,238, 0)' // 0% 处的颜色
                                    }, {
                                        offset: 1,
                                        color: 'rgba(	47,79,79, .2)' // 100% 处的颜色
                                    }],
                                    globalCoord: false // 缺省为 false
                                },
                                shadowColor: 'rgba(128, 217, 248, 1)',
                                shadowOffsetX: -2,
                                shadowOffsetY: 2,
                                shadowBlur: 10
                            },
                            emphasis: {
                                areaColor: '#389BB7',
                                borderWidth: 0
                            }
                        }
                    },
                    series: [{
                            name: 'light',
                            type: 'map',
                            coordinateSystem: 'geo',
                            data: convertData(data),
                            itemStyle: {
                                normal: {
                                    color: '#F4E925'
                                }
                            }
                        },
                        {
                            name: '点',
                            type: 'scatter',
                            coordinateSystem: 'geo',
                            symbol: 'pin',
                            symbolSize: function(val) {
                                var a = (maxSize4Pin - minSize4Pin) / (max - min);
                                var b = minSize4Pin - a * min;
                                b = maxSize4Pin - a * max;
                                return a * val[2] + b;
                            },
                            label: {
                                normal: {
                                    // show: true,
                                    // textStyle: {
                                    //     color: '#fff',
                                    //     fontSize: 9,
                                    // }
                                }
                            },
                            itemStyle: {
                                normal: {
                                    color: '#F62157', //标志颜色
                                }
                            },
                            zlevel: 6,
                            data: convertData(data),
                        },
                        {  
                            name: 'light',
                            type: 'map',
                            mapType: 'hunan',
                            geoIndex: 0,
                            aspectScale: 0.75, //长宽比
                            showLegendSymbol: false, // 存在legend时显示
                            label: {
                                normal: {
                                    show: false
                                },
                                emphasis: {
                                    show: false,
                                    textStyle: {
                                        color: '#fff'
                                    }
                                }
                            },
                            roam: true,
                            itemStyle: {
                                normal: {
                                    areaColor: '#031525',
                                    borderColor: '#FFFFFF',
                                },
                                emphasis: {
                                    areaColor: '#2B91B7'
                                }
                            },
                            animation: false,
                            data: data
                        },
                        {
                            name: ' ',
                            type: 'effectScatter',
                            coordinateSystem: 'geo',
                            data: convertData(data.sort(function (a, b) {
                                return b.value - a.value;
                            }).slice(0, 5)),
                            symbolSize: function (val) {
                                return val[2] / 10;
                            },
                            showEffectOn: 'render',
                            rippleEffect: {
                                brushType: 'stroke'
                            },
                            hoverAnimation: true,
                            label: {
                                normal: {
                                    formatter: '{b}',
                                    position: 'right',
                                    show: true
                                }
                            },
                            itemStyle: {
                                normal: {
                                    color: '#05C3F9',
                                    shadowBlur: 10,
                                    shadowColor: '#05C3F9'
                                }
                            },
                            zlevel: 1
                        },

                    ]
                });
        }
        showProvince();

        // 使用刚指定的配置项和数据显示图表。
        // myChart.setOption(option);
        window.addEventListener("resize", function () {
            myChart.resize();
        });
    }
    //点击跳转
    // $('#chart_map').click(function(){
    //     window.location.href = './page/index.html';
    // });
    // $('.t_btn2').click(function(){
    //     window.location.href = "./page/index.html?id=2";
    // });
    // $('.t_btn3').click(function(){
    //     window.location.href = "./page/index.html?id=3";
    // });
    // $('.t_btn4').click(function(){
    //     window.location.href = "./page/index.html?id=4";
    // });
    // $('.t_btn5').click(function(){
    //     window.location.href = "./page/index.html?id=5";
    // });
    // $('.t_btn6').click(function(){
    //     window.location.href = "./page/index.html?id=6";
    // });
    // $('.t_btn7').click(function(){
    //     window.location.href = "./page/index.html?id=7";
    // });
    // $('.t_btn8').click(function(){
    //     window.location.href = "./page/index.html?id=8";
    // });
    // $('.t_btn9').click(function(){
    //     window.location.href = "./page/index.html?id=9";
    // });
});
